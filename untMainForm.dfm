object MainForm: TMainForm
  Left = 327
  Top = 285
  BorderStyle = bsDialog
  Caption = 'MainForm'
  ClientHeight = 142
  ClientWidth = 270
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 16
    Top = 20
    Width = 109
    Height = 13
    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1087#1086#1090#1086#1082#1086#1074':'
    FocusControl = seThreads
  end
  object lbl2: TLabel
    Left = 16
    Top = 48
    Width = 109
    Height = 13
    Caption = #1054#1075#1088#1072#1085#1080#1095#1077#1085#1080#1077' '#1089#1074#1077#1088#1093#1091':'
    FocusControl = seSimpleLimit
  end
  object lblStatus: TLabel
    Left = 16
    Top = 80
    Width = 237
    Height = 13
    Alignment = taCenter
    AutoSize = False
    Caption = #1053#1072#1078#1084#1080#1090#1077' '#1082#1085#1086#1087#1082#1091' "'#1057#1090#1072#1088#1090'"'
  end
  object seThreads: TSpinEdit
    Left = 132
    Top = 16
    Width = 121
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 0
    Value = 2
  end
  object seSimpleLimit: TSpinEdit
    Left = 132
    Top = 44
    Width = 121
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 1
    Value = 1000000
  end
  object btnStart: TButton
    Left = 16
    Top = 108
    Width = 237
    Height = 25
    Caption = #1057#1090#1072#1088#1090
    TabOrder = 2
    OnClick = btnStartClick
  end
end
