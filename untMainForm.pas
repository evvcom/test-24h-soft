unit untMainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin,
  untThread;

type
  TMainForm = class(TForm)
    lbl1: TLabel;
    seThreads: TSpinEdit;
    lbl2: TLabel;
    seSimpleLimit: TSpinEdit;
    btnStart: TButton;
    lblStatus: TLabel;
    procedure btnStartClick(Sender: TObject);
  private
    FThreadManager: TSimpleThreadManager;
    procedure ThreadManagerComplete(Sender: TObject);
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.btnStartClick(Sender: TObject);
begin
  btnStart.Enabled := False;
  lblStatus.Caption := '����������� ������� �����';
  FThreadManager := TSimpleThreadManager.Create(seThreads.Value, seSimpleLimit.Value);
  FThreadManager.OnComplete := ThreadManagerComplete;
  FThreadManager.Resume;
end;

procedure TMainForm.ThreadManagerComplete(Sender: TObject);
begin
  FThreadManager.Free;
  FThreadManager := nil;
  btnStart.Enabled := True;
  lblStatus.Caption := '������� ����� �������!';
end;

end.
