unit untThread;

interface

uses
  Classes, Windows, Messages, SyncObjs, SysUtils;
  
type
  TByteArray = array of Byte;

  TSimpleThreadManager = class
  private
    // ������ ����������
    FEratosthenSieve: TByteArray;
    FLastSimple: Integer;
    // ������
    FThreads: array of TThread;
    FActiveThreads: Integer;
    // ������� �������������
    FEratosthenCriticalSection: TCriticalSection; // ������ ������
    FThreadsCriticalSection:    TCriticalSection; // ������ ������� �������
    // ���� ��� ������ ����������� ��������� �� �������
    FWindowHandle: HWND;
    // �����
    FTextFiles: array of TextFile;
    // �������
    FOnComplete: TNotifyEvent; 

    procedure WndProc(var Msg: TMessage);
    procedure SimpleNumber(AThreadNum, ANumber: Integer);
    procedure ThreadTerminate(Sender: TObject);
    function GetMinStriked: Integer;
  public
    constructor Create(AThreadCount, ASimpleMax: Integer);
    destructor Destroy; override;
    procedure Resume;
  published
    property OnComplete: TNotifyEvent read FOnComplete write FOnComplete;
  end;

implementation

uses
  Forms, Math;

const
  SIMPLE_FLAG:    Byte = 0;
  STRIKEOUT_FLAG: Byte = $FF;

  WM_SIMPLE_NUMBER: DWORD = WM_USER + 0;

type
  TSimpleThread = class(TThread)
  private
    FOwner: TSimpleThreadManager;
    FThreadNum: Integer;
    FStriked: Integer;
  protected
    procedure Execute; override;
  public
    constructor Create(AOmner: TSimpleThreadManager; ANum: Integer);
  end;

{ TSimpleThread }

constructor TSimpleThread.Create(AOmner: TSimpleThreadManager; ANum: Integer);
begin
  FOwner := AOmner;
  FThreadNum := ANum;
  FreeOnTerminate := True;
  OnTerminate := AOmner.ThreadTerminate;
  FStriked := Length(FOwner.FEratosthenSieve)-1;
  inherited Create(True);
end;

procedure TSimpleThread.Execute;
var
  criticalSection: TCriticalSection;
  eratosthenSieve: TByteArray;
  lastSimple: Integer;
  maxNumber: Integer;
  num: Integer;
begin
  criticalSection := FOwner.FEratosthenCriticalSection;
  eratosthenSieve := FOwner.FEratosthenSieve;
  maxNumber := Length(eratosthenSieve)-1;

  // ����������� ����
  while True do
  begin
    // ����� �������� - �������� �����
    criticalSection.Enter;
    try
      lastSimple := FOwner.FLastSimple;
      repeat
        Inc(lastSimple);
        if lastSimple > maxNumber then Exit;
        // ����� ����� ���������� ���, ��� ������ �����,
        // ������� ������� ����� � ������� � ������������,
        // ��� ���������� ��������.
        // ���� ����� ����� ���� ������, ��� ������� ������������� �����
        // �������� ������� ������, ���� ������ ������,
        // ���������� ����� � �������, ������ ������ ����� �����.
        while lastSimple > FOwner.GetMinStriked do
          Sleep(0);
      until eratosthenSieve[lastSimple] = SIMPLE_FLAG;
      FOwner.FLastSimple := lastSimple;
      // ������� ����� �������, ������� ��������� � ��������� ���� ������
      PostMessage(FOwner.FWindowHandle, WM_SIMPLE_NUMBER, FThreadNum, lastSimple);
    finally
      criticalSection.Leave;
    end;
    // ������������ �����, �� ���������� ��������
    num := lastSimple*2;
    while num <= maxNumber do
    begin
      // ����������� ����� �� ������ ����� ��� �������������
      eratosthenSieve[num] := STRIKEOUT_FLAG;
      FStriked := num;
      Inc(num, lastSimple);
    end;
    FStriked := maxNumber;
  end;
end;

{ TThreadManager }

constructor TSimpleThreadManager.Create(AThreadCount, ASimpleMax: Integer);
var
  i: Integer;
  fileName: string;
begin
  // ������ ����������
  SetLength(FEratosthenSieve, ASimpleMax+1);
  FLastSimple := 1;
  // ������
  SetLength(FThreads, AThreadCount);
  for i := 1 to AThreadCount do
    FThreads[i-1] := TSimpleThread.Create(Self, i);
  FActiveThreads := AThreadCount;
  // ���������������� �������
  FEratosthenCriticalSection := TCriticalSection.Create;
  FThreadsCriticalSection := TCriticalSection.Create;
  // ���� ��� ������ ����������� ��������� �� �������
  FWindowHandle := Classes.AllocateHWnd(WndProc);
  // �����
  SetLength(FTextFiles, AThreadCount+1);
  for i := 0 to AThreadCount do
  begin
    if i = 0 then
      fileName := 'Result.txt'
    else
      fileName := 'Thread' + IntToStr(i) + '.txt';
    AssignFile(FTextFiles[i], fileName);
    Rewrite(FTextFiles[i]);
  end;
end;

destructor TSimpleThreadManager.Destroy;
begin
  Classes.DeallocateHWnd(FWindowHandle);
  FEratosthenCriticalSection.Free;
  FThreadsCriticalSection.Free;
  inherited Destroy;
end;

procedure TSimpleThreadManager.Resume;
var
  i: Integer;
begin
  for i := 0 to Length(FThreads)-1 do
    FThreads[i].Resume;
end;

procedure TSimpleThreadManager.WndProc(var Msg: TMessage);
begin
  with Msg do
    if Msg = WM_SIMPLE_NUMBER then
      try
        SimpleNumber(wParam, lParam);
      except
        Application.HandleException(Self);
      end
    else
      Result := DefWindowProc(FWindowHandle, Msg, wParam, lParam);
end;

procedure TSimpleThreadManager.SimpleNumber(AThreadNum, ANumber: Integer);
begin
  Write(FTextFiles[0], ANumber, ' ');
  Write(FTextFiles[AThreadNum], ANumber, ' ');
end;

procedure TSimpleThreadManager.ThreadTerminate(Sender: TObject);
var
  threadNum: Integer;
begin
  threadNum := TSimpleThread(Sender).FThreadNum;
  CloseFile(FTextFiles[threadNum]);

  FThreadsCriticalSection.Enter;
  FThreads[threadNum-1] := nil;
  FThreadsCriticalSection.Leave;

  Dec(FActiveThreads);
  if FActiveThreads = 0 then
  begin
    CloseFile(FTextFiles[0]);
    if Assigned(FOnComplete) then
      FOnComplete(Self);
  end;
end;

function TSimpleThreadManager.GetMinStriked: Integer;
var
  i: Integer;
begin
  Result := Length(FEratosthenSieve)-1;
  FThreadsCriticalSection.Enter;
  for i := 0 to Length(FThreads)-1 do
    if Assigned(FThreads[i]) then
      Result := Min(Result, TSimpleThread(FThreads[i]).FStriked);
  FThreadsCriticalSection.Leave;
end;

end.
